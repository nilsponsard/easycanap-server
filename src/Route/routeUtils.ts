import {Context} from 'https://deno.land/x/oak/mod.ts';

/**
 * Sends a JSON-formatted response to a request.
 *
 * @param ctx Request context
 * @param response Response object
 */
export const sendJSONResponse = <T extends object>(ctx: Context<Record<string, any>>, response: T): void => {
    // Set content type to application/json and stringify the response object
    ctx.response.headers.append('Content-Type', 'application/json');
    ctx.response.body = JSON.stringify(response);
}
