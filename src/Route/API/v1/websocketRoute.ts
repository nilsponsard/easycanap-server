import {RouterContext} from 'https://deno.land/x/oak/mod.ts';

/**
 * This endpoint fetches the URL to the websocket server.
 *
 * ## Request
 * - API Version: 1
 * - Endpoint: ``/ws``
 * - Method: ``GET``
 *
 * ## Response
 * The response is an URL to the websocket server, or nothing and an HTTP response code != 200 if an error occurred.
 *
 * @param ctx Request context
 */
const websocketRoute = async (ctx: RouterContext<Record<string | number, string | undefined>, Record<string, any>>): Promise<void> => {
    if (ctx.isUpgradable) {
        return ctx.upgrade()
            .then(ws => {
                ws.send('test');
            });
    } else {
        // Request is not upgradable, tell the client
        ctx.response.status = 426;
        ctx.response.headers.append('Upgrade', 'websocket');
        ctx.response.headers.append('Connection', 'Upgrade');

        ctx.response.body = 'This endpoint requires upgrading to websocket.';
    }
};

export default websocketRoute;
