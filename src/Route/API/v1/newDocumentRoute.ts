import {RouterContext} from 'https://deno.land/x/oak/mod.ts';
import {v4} from 'https://deno.land/std@0.85.0/uuid/mod.ts';
import {ensureDir} from 'https://deno.land/std@0.85.0/fs/ensure_dir.ts';
import {documentsPath} from '../../../constants.ts';
import {sendJSONResponse} from '../../routeUtils.ts';

/**
 *
 * just send the id of the message an the error status
 */
interface DocumentIdResponse {
    error: boolean
    id: string
}

/**
 * Endpoint /api/v1/new
 *
 * API Endpoint for creating a document and get the id of the document
 *
 * @param ctx Request context
 */
const newDocumentRoute = (ctx: RouterContext<Record<string | number, string | undefined>, Record<string, any>>): Promise<void> => {

    return ensureDir(documentsPath)
        .then(() => {
            const id = v4.generate();
            Deno.writeTextFileSync(documentsPath + id, '');
            sendJSONResponse<DocumentIdResponse>(ctx, {error: false, id: id});
        })
        .catch(reason => {
            sendJSONResponse<DocumentIdResponse>(ctx, {error: true, id: '0'});
        });

};

export default newDocumentRoute;
