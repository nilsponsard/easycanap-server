import {RouterContext, send} from 'https://deno.land/x/oak/mod.ts';
import {documentsPath} from '../../../constants.ts';

/**
 * Interface for the route params passed to the endpoint
 */
interface DocumentFetchRouteParams extends Record<string | number, string | undefined> {
    id: string,
}

/**
 * This endpoint fetches the document with the ID specified in the route.
 *
 * ## Request
 * - API Version: 1
 * - Endpoint: ``/document/:id``
 * - Method: ``GET``
 * - Parameters:
 * - ``:id``: <string>, ID of the document to fetch
 *
 * ## Response
 * The response is the raw document, or nothing and an HTTP response code != 200 if an error occurred.
 *
 * @param ctx Request context
 */
const documentFetchRoute = async (ctx: RouterContext<DocumentFetchRouteParams, Record<string, any>>): Promise<void> => {
    // Simply serve the requested file
    return send(ctx, ctx.params.id, {
        root: documentsPath,
    })
        .then(() => {
            // We're returning a Promise<void>
        })
        .catch(() => {
            // Don't put anything in the response body when an exception occurred
        });
};

export default documentFetchRoute;
