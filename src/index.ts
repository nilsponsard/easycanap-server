import {Application, Router, send} from 'https://deno.land/x/oak/mod.ts';
import documentFetchRoute from './Route/API/v1/documentFetchRoute.ts';
import newDocumentRoute from './Route/API/v1/newDocumentRoute.ts';
import websocketRoute from './Route/API/v1/websocketRoute.ts';

const files_path = `${Deno.cwd()}/files/`;

// Instantiate and setup the router
const router = new Router();
router
    .prefix('/api/v1')
    .get('/test', (ctx) => {
        // Send a JSON response
        ctx.response.headers.append('Content-Type', 'application/json');
        ctx.response.body = JSON.stringify({
            error: 0,
            text: 'Hello, World!',
        });
    })
    .get('/echo/:text', (ctx) => {
        // Send back what we received in the route
        if (ctx.params && ctx.params.text) {
            ctx.response.body = ctx.params.text;
        }
    })
    .post('/new', newDocumentRoute)
    .get('/document/:id', documentFetchRoute)
    .get('/ws', websocketRoute);

// Instantiate the server, let the router handle routing except
// if there's a file corresponding to the request path in /build
const app = new Application();

app.use(router.routes());
app.use(router.allowedMethods());

app.use(async (ctx) => {
    await send(ctx, ctx.request.url.pathname, {
        root: `${Deno.cwd()}/build`,
        index: 'index.html',
    });
});

// Log something when the server is open
app.addEventListener('listen', ({hostname, port, secure}) => {
    console.log('Server is running.');
    console.log(`> ${secure ? 'https://' : 'http://'}${hostname ?? 'localhost'}:${port}`);
});

// Start the server
app.listen({port: 8081});
